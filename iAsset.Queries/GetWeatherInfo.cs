﻿using iAsset.Entities;
using iAsset.WebServices;

namespace iAsset.Queries
{
    public class GetWeatherInfo : IGetWeatherInfo
    {
        private readonly IWeatherWebService _weatherWebService;

        public GetWeatherInfo(IWeatherWebService weatherWebService)
        {
            _weatherWebService = weatherWebService;
        }

        public Weather Execute(int cityId)
        {
            return _weatherWebService.Get(cityId);
        }
    }
}