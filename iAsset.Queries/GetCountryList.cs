﻿using System.Collections.Generic;
using System.Linq;
using iAsset.Entities;
using iAsset.WebServices;

namespace iAsset.Queries
{
    public class GetCountryList : IGetCountryList
    {
        private readonly ICountryWebService _countryWebService;

        public GetCountryList(ICountryWebService countryWebService)
        {
            _countryWebService = countryWebService;
        }

        public IEnumerable<Country> Execute(string query)
        {
            var countryList = _countryWebService.Get();
            return countryList.Where(x => x.Name.ToUpperInvariant().Contains(query.ToUpperInvariant()));            
        }
    }
}