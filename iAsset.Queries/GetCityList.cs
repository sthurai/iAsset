﻿using System.Collections.Generic;
using System.Linq;
using iAsset.Entities;
using iAsset.WebServices;

namespace iAsset.Queries
{
    public class GetCityList : IGetCityList
    {
        private readonly ICityWebService _cityWebService;

        public GetCityList(ICityWebService cityWebService)
        {
            _cityWebService = cityWebService;
        }

        public IEnumerable<City> Execute(string countryCode, string query)
        {
            var cityList =  _cityWebService.Get(countryCode);
            return cityList.Where(x => x.Name.ToUpperInvariant().Contains(query.ToUpperInvariant()));
        }
    }
}