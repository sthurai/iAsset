﻿using iAsset.Entities;

namespace iAsset.Queries
{
    public interface IGetWeatherInfo
    {
        Weather Execute(int cityId);
    }
}