﻿using System.Collections.Generic;
using iAsset.Entities;

namespace iAsset.Queries
{
    public interface IGetCountryList
    {
        IEnumerable<Country> Execute(string query);
    }
}