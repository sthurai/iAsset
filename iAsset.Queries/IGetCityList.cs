﻿using System.Collections.Generic;
using iAsset.Entities;

namespace iAsset.Queries
{
    public interface IGetCityList
    {
        IEnumerable<City> Execute(string countryCode, string query);
    }
}