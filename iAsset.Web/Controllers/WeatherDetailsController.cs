﻿using System.Web.Http;
using iAsset.DTOs;
using iAsset.Services;

namespace iAsset.Web.Controllers
{
    [RoutePrefix("api/v1/weather")]
    public class WeatherDetailsController : ApiController
    {
        private readonly IWeatherService _weatherService;

        public WeatherDetailsController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [Route("{id:int}")]
        public WeatherDto Get(int id)
        {
            return _weatherService.Get(id);
        }        
    }
}
