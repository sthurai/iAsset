﻿using System.Collections.Generic;
using System.Web.Http;
using iAsset.DTOs;
using iAsset.Services;

namespace iAsset.Web.Controllers
{
    [RoutePrefix("api/v1/city")]
    public class CityListController : ApiController
    {
        private readonly ICityListService _cityListService;

        public CityListController(ICityListService cityListService)
        {
            _cityListService = cityListService;
        }

        [Route("{countryCode}/{query}")]        
        public IEnumerable<CityDto> Get(string countryCode, string query)
        {
            return _cityListService.Get(countryCode, query);
        }        
    }
}
