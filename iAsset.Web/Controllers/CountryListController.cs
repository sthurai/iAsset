﻿using System.Collections.Generic;
using System.Web.Http;
using iAsset.DTOs;
using iAsset.Services;

namespace iAsset.Web.Controllers
{
    [RoutePrefix("api/v1/country")]
    public class CountryListController : ApiController
    {
        private readonly ICountryListService _countryListService;

        public CountryListController(ICountryListService countryListService)
        {
            _countryListService = countryListService;
        }

        [Route("list/{query}")]
        public IEnumerable<CountryDto> Get(string query)
        {
            return _countryListService.Get(query);
        }      
    }
}
