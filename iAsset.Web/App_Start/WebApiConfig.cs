﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using iAsset.Web.Infrastructure;

namespace iAsset.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/v1/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Services.Add(typeof(IExceptionLogger), new GlobalExceptionLogger());
        }
    }
}
