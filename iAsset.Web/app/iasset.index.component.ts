﻿namespace App {

    class WeatherAppController implements ng.IComponentController {
        public selectedCountry: any;
        public selectedCity: any;
        public weatherInfo: any;

        private $http: ng.IHttpService;

        static $inject: Array<string> = ['$http'];

        constructor($http: ng.IHttpService) {
            this.$http = $http;
        }

        public getCountryList(searchText: string) {            
            return this.$http.get(`/api/v1/country/list/${searchText}`).then(response => {
                return response.data;                
            });            
        }

        public getCityList(searchText: string) {            
            return this.$http.get(`/api/v1/city/${this.selectedCountry.Code}/${searchText}`).then(response => {
                return response.data;                
            });            
        }

        public getWeatherInfo() {
            this.$http.get(`/api/v1/weather/${this.selectedCity.Id}`).then(response => {
                this.weatherInfo = response.data;                
            });            
        }
    }

    class WeatherApp implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;        

        constructor() {
            this.controller = WeatherAppController;
            this.templateUrl = '/app.html';            
        }

        static getInstance() {
            return new WeatherApp();
        }
    }

    angular.module('iasset').component('weatherApp', WeatherApp.getInstance());
}