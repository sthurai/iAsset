var App;
(function (App) {
    var WeatherAppController = (function () {
        function WeatherAppController($http) {
            this.$http = $http;
        }
        WeatherAppController.prototype.getCountryList = function (searchText) {
            return this.$http.get("/api/v1/country/list/" + searchText).then(function (response) {
                return response.data;
            });
        };
        WeatherAppController.prototype.getCityList = function (searchText) {
            return this.$http.get("/api/v1/city/" + this.selectedCountry.Code + "/" + searchText).then(function (response) {
                return response.data;
            });
        };
        WeatherAppController.prototype.getWeatherInfo = function () {
            var _this = this;
            this.$http.get("/api/v1/weather/" + this.selectedCity.Id).then(function (response) {
                _this.weatherInfo = response.data;
            });
        };
        return WeatherAppController;
    }());
    WeatherAppController.$inject = ['$http'];
    var WeatherApp = (function () {
        function WeatherApp() {
            this.controller = WeatherAppController;
            this.templateUrl = '/app.html';
        }
        WeatherApp.getInstance = function () {
            return new WeatherApp();
        };
        return WeatherApp;
    }());
    angular.module('iasset').component('weatherApp', WeatherApp.getInstance());
})(App || (App = {}));
//# sourceMappingURL=iasset.index.component.js.map