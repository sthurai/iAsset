﻿using System.Web.Http.ExceptionHandling;
using NLog;

namespace iAsset.Web.Infrastructure
{
    public class GlobalExceptionLogger: ExceptionLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Error(context.ExceptionContext.Exception);
        }
    }
}
