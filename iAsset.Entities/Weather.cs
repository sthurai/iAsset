﻿using System;

namespace iAsset.Entities
{
    public class Weather
    {
        public string Location { get; set; }
        public DateTime Time { get; set; }
        public int  Wind { get; set; }
        public int Visibility { get; set; }
        public string SkyConditions { get; set; }
        public int Temperature { get; set; }
        public int DewPoint { get; set; }
        public int RelativeHumidity { get; set; }
        public int Pressure { get; set; }
    }
}
