﻿using System.Collections.Generic;
using FizzWare.NBuilder;
using FluentAssertions;
using iAsset.Entities;
using iAsset.WebServices;
using Moq;
using Xunit;

namespace iAsset.Queries.Tests
{
    public class GetCityListTests
    {
        private readonly GetCityList _query;
        public GetCityListTests()
        {
            var mockCityWebService = new Mock<ICityWebService>();
            mockCityWebService.Setup(x => x.Get(It.IsAny<string>())).Returns(GetCities());
            _query = new GetCityList(mockCityWebService.Object);
        }

        [Fact]
        public void CheckIfExecuteReturningCorrectValue()
        {            
            var result = _query.Execute("au", "name");
            result.Should().HaveCount(20);
        }

        [Fact]
        public void CheckIfExecuteReturningNothingForIncorrectName()
        {
            var result = _query.Execute("au", "IncorrectName");
            result.Should().HaveCount(0);
        }

        private IEnumerable<City> GetCities()
        {
            return Builder<City>.CreateListOfSize(20).Build();
        }
    }
}
