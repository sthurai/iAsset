﻿using iAsset.DTOs;

namespace iAsset.Services
{
    public interface IWeatherService
    {
        WeatherDto Get(int cityId);
    }
}