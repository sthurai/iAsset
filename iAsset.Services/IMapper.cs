﻿namespace iAsset.Services
{
    public interface IMapper
    {
        TDestination Adapt<TSource, TDestination>(TSource source);
    }
}