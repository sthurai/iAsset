﻿using iAsset.DTOs;
using iAsset.Entities;
using iAsset.Queries;

namespace iAsset.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IGetWeatherInfo _getWeatherInfo;
        private readonly IMapper _mapper;

        public WeatherService(IGetWeatherInfo getWeatherInfo, IMapper mapper)
        {
            _getWeatherInfo = getWeatherInfo;
            _mapper = mapper;
        }

        public WeatherDto Get(int cityId)
        {
            var weatherInfo = _getWeatherInfo.Execute(cityId);
            return _mapper.Adapt<Weather, WeatherDto>(weatherInfo);
        }
    }
}