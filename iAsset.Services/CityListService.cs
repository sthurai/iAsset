﻿using System.Collections.Generic;
using iAsset.DTOs;
using iAsset.Entities;
using iAsset.Queries;

namespace iAsset.Services
{
    public class CityListService : ICityListService
    {
        private readonly IGetCityList _getCityList;
        private readonly IMapper _mapper;

        public CityListService(IGetCityList getCityList, IMapper mapper)
        {
            _getCityList = getCityList;
            _mapper = mapper;
        }

        public IEnumerable<CityDto> Get(string countryCode, string query)
        {
            var cities = _getCityList.Execute(countryCode, query);
            return _mapper.Adapt<IEnumerable<City>, IEnumerable<CityDto>>(cities);
        }
    }
}