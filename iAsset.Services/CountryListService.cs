﻿using System.Collections.Generic;
using iAsset.DTOs;
using iAsset.Entities;
using iAsset.Queries;

namespace iAsset.Services
{
    public class CountryListService : ICountryListService
    {
        private readonly IGetCountryList _getCountryList;
        private readonly IMapper _mapper;

        public CountryListService(IGetCountryList getCountryList, IMapper mapper)
        {
            _getCountryList = getCountryList;
            _mapper = mapper;
        }

        public IEnumerable<CountryDto> Get(string query)
        {
            var countries =  _getCountryList.Execute(query);
            return _mapper.Adapt<IEnumerable<Country>, IEnumerable<CountryDto>>(countries);            
        }
    }
}
