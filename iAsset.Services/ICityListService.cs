﻿using System.Collections.Generic;
using iAsset.DTOs;

namespace iAsset.Services
{
    public interface ICityListService
    {
        IEnumerable<CityDto> Get(string countryCode, string query);
    }
}