﻿using System.Collections.Generic;
using iAsset.DTOs;

namespace iAsset.Services
{
    public interface ICountryListService
    {
        IEnumerable<CountryDto> Get(string query);
    }
}