﻿using FastMapper;

namespace iAsset.Services
{
    public class Mapper : IMapper
    {
        public TDestination Adapt<TSource, TDestination>(TSource source)
        {
            return TypeAdapter.Adapt<TSource, TDestination>(source);
        }
    }
}