﻿
using System.Linq;
using SimpleInjector;

namespace iAsset.Services
{
    public static class RegisterDependency
    {
        public static void Execute(Container container)
        {
            var repositoryAssembly = typeof(CountryListService).Assembly;

            var registrations =
                from type in repositoryAssembly.GetExportedTypes()
                where type.Namespace == "iAsset.Services"
                where type.GetInterfaces().Any()
                select new { Service = type.GetInterfaces().Single(), Implementation = type };

            foreach (var reg in registrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }

            Queries.RegisterDependency.Execute(container);
            WebServices.RegisterDependency.Execute(container);
        }
    }
}
