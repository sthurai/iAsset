﻿using System.Collections.Generic;
using FizzWare.NBuilder;
using iAsset.Entities;

namespace iAsset.WebServices
{
    public class CityWebServiceMock : ICityWebService
    {
        private const int NoOfCities = 25;
        public IEnumerable<City> Get(string countryCode)
        {
            return Builder<City>.CreateListOfSize(NoOfCities).Build();
        }
    }
}