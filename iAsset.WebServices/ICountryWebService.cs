﻿using System.Collections.Generic;
using iAsset.Entities;

namespace iAsset.WebServices
{
    public interface ICountryWebService
    {
        IEnumerable<Country> Get();
    }
}