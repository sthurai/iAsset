﻿using FizzWare.NBuilder;
using iAsset.Entities;

namespace iAsset.WebServices
{
    public class WeatherWebService : IWeatherWebService
    {
        public Weather Get(int cityId)
        {
            return Builder<Weather>.CreateNew().Build();
        }
    }
}