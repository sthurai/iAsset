﻿using iAsset.Entities;

namespace iAsset.WebServices
{
    public interface IWeatherWebService
    {
        Weather Get(int cityId);
    }
}