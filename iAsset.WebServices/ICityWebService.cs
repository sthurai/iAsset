﻿using System.Collections.Generic;
using iAsset.Entities;

namespace iAsset.WebServices
{
    public interface ICityWebService
    {
        IEnumerable<City> Get(string countryCode);
    }
}