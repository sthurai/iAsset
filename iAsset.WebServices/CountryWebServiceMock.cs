﻿using System.Collections.Generic;
using System.Linq;
using iAsset.Entities;

namespace iAsset.WebServices
{
    public class CountryWebServiceMock : ICountryWebService
    {
        public IEnumerable<Country> Get()
        {
            return new List<Country>
            {
                new Country { Name = "Australia", Code = "au"},
                new Country { Name = "United States Of America", Code = "us"},
                new Country { Name = "United Kingdom", Code = "uk"},
                new Country { Name = "New Zealand", Code = "nz"}
            };
        }       
    }
}