﻿using System.Linq;
using SimpleInjector;

namespace iAsset.WebServices
{
    public static class RegisterDependency
    {
        public static void Execute(Container container)
        {
            var repositoryAssembly = typeof(CountryWebServiceMock).Assembly;

            var registrations =
                from type in repositoryAssembly.GetExportedTypes()
                where type.Namespace == "iAsset.WebServices"
                where type.GetInterfaces().Any()
                select new { Service = type.GetInterfaces().Single(), Implementation = type };

            foreach (var reg in registrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }
        }
    }
}
